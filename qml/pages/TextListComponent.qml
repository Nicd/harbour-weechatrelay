import QtQuick 2.0
import Sailfish.Silica 1.0

import "../js/utils.js" as U

SilicaListView {
    id: textList

    function colorTime(time) {
        return U.colored(Theme.secondaryHighlightColor,
                         time);
    }

    verticalLayoutDirection: ListView.BottomToTop

    delegate: ListItem {
        width: parent.width
        contentHeight: messageLabel.height

        Label {
            id: messageLabel
            text: colorTime(time) + " " + U.bold(U.escapeStyled(prefix)) + " " + U.escapeStyled(str)
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            color: Theme.highlightColor

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: Theme.paddingMedium
            anchors.rightMargin: Theme.paddingMedium

            textFormat: Text.StyledText
        }
    }

    VerticalScrollDecorator { flickable: textList }
}
