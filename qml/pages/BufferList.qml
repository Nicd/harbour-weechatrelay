/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

import "../js/buffers.js" as B

Page {
    id: bufferListPage

    property BufferView bufferViewPage : null;

    SilicaListView {
        id: bufferList
        anchors.fill: parent

        model: ListModel {}

        Component.onCompleted: {
            // Get bufferlistmodel from buffers.js
            bufferList.model = B.getBufferListModel(bufferListPage);
        }

        header: PageHeader {
            width: bufferList.width
            title: "Buffers"
        }

        delegate: ListItem {
            id: listItem
            contentHeight: Theme.itemSizeExtraSmall
            menu: contextMenu
            ListView.onRemove: animateRemoval(listItem);

            function close() {
                remorseAction("Closing buffer",
                              function() {
                                  // TODO Close buffer forrealz
                                  console.log("Closed " + name + "!");
                              });
            }

            GlassItem {
                id: readIndicator
                opacity: unreadCount > 0 ? 1.0 : 0.0;

                anchors.left: parent.left
                anchors.leftMargin: -Theme.paddingLarge
                //anchors.top: parent.top
                //anchors.topMargin: -Theme.paddingLarge
                //anchors.bottom: parent.bottom
                anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                id: bufferNumber
                text: number
                color: listItem.highlighted ? Theme.secondaryHighlightColor
                                            : Theme.secondaryColor;

                anchors {
                    left: readIndicator.right
                    leftMargin: -2 * Theme.paddingLarge
                    verticalCenter: parent.verticalCenter
                }

                width: Theme.itemSizeSmall
                horizontalAlignment: Qt.AlignRight
            }

            Label {
                id: bufferName
                text: name
                color: listItem.highlighted ? Theme.highlightColor
                                            : Theme.primaryColor;

                anchors {
                    left: bufferNumber.right
                    leftMargin: Theme.paddingLarge
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
            }

            Component {
                id: contextMenu
                ContextMenu {
                    MenuItem {
                        text: "Close"
                        onClicked: close();
                    }
                }
            }

            onClicked: {
                var buffer = B.getBuffer(pointer);
                bufferViewPage.changeBuffer(buffer);
                pageStack.navigateBack(PageStackAction.Animated);
            }
        }

        VerticalScrollDecorator { flickable: connectionList }


        PullDownMenu {
            MenuItem {
                text: "???"
            }
        }

        PushUpMenu {
            MenuItem {
                text: "Go to top"
                onClicked: bufferList.scrollToTop();
            }
        }
    }
}
