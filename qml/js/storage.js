.pragma library

/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

.import QtQuick.LocalStorage 2.0 as LS

var db_inst = null;

function connect() {
    // Connect if not already connected, otherwise just return instance
    if (db_inst === null) {
        db_inst = LS.LocalStorage.openDatabaseSync("WeeCRApp", "1.0", "StorageDatabase", 10240);

        db_inst.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS settings \
                            (key TEXT PRIMARY KEY, \
                            value TEXT);");

            tx.executeSql("CREATE TABLE IF NOT EXISTS connections \
                            (id INTEGER PRIMARY KEY, \
                            name TEXT, \
                            host TEXT, \
                            port INTEGER, \
                            password TEXT, \
                            type TEXT, \
                            options TEXT \
                            );");
        });
    }

    return db_inst;
}

function readSetting(db, key, defVal) {
    var setting = null;

    db.readTransaction(function(tx) {
        var rows = tx.executeSql("SELECT value AS val FROM settings WHERE key=?;", [key]);

        if (rows.rows.length !== 1) {
            setting = null;
        }
        else {
            setting = rows.rows.item(0).val;
        }
    });

    if (setting === 'true') {
        setting = true;
    }
    else if (setting === 'false') {
        setting = false;
    }
    // If setting has never been read (doesn't exist), use default value
    else if (setting === null) {
        setting = defVal;
    }

    return setting;
}

function storeSetting(db, key, value) {
    if (value === true) {
        value = 'true';
    }
    else if (value === false) {
        value = 'false';
    }

    db.transaction(function(tx) {
        tx.executeSql("INSERT OR REPLACE INTO settings VALUES (?, ?);", [key, value]);
        tx.executeSql("COMMIT;");
    });
}



// Connection class
function Connection(infodict) {
    this.id = infodict.id;
    this.name = infodict.name;
    this.host = infodict.host;
    this.port = infodict.port;
    this.password = infodict.password;
    this.type = infodict.type;

    // If infodict.options is not an array, try to parse it as JSON
    if (infodict.options === null) {
        this.options = {};
    }
    else if (typeof infodict.options === 'object') {
        this.options = infodict.options;
    }
    else {
        this.options = JSON.parse(infodict.options);
    }
}

// Convert connection back to object suitable for storage in database
Connection.prototype.toStorageDict = function() {
    return {
        "id": this.id,
        "name": this.name,
        "host": this.host,
        "port": this.port,
        "password": this.password,
        "type": this.type,
        "options": JSON.stringify(this.options)
    };
};


function readAllConnections(db) {
    var connlist = [];
    var ids = [];

    db.readTransaction(function(tx) {
        var rows = tx.executeSql("SELECT id FROM connections;");
        for (var i = 0; i < rows.rows.length; ++i) {
            ids.push(rows.rows.item(i).id);
        }
    });

    for (var i = 0; i < ids.length; ++i) {
        connlist.push(readConnection(db, ids[i]));
    }

    return connlist;
}

function readConnection(db, id) {
    var infodict = null;

    db.readTransaction(function(tx) {
        var rows = tx.executeSql("SELECT id, name, host, port, password, type, options \
                                    FROM connections WHERE id = ?;", [id]);

        if (rows.rows.length === 1) {
            infodict = rows.rows.item(0);
        }
    });

    if (infodict !== null) {
        return new Connection(infodict);
    }

    return null;
}

function storeConnection(db, connection) {
    var infodict = connection.toStorageDict();

    db.transaction(function(tx) {
        tx.executeSql("INSERT OR REPLACE INTO connections \
                        (id, name, host, port, password, type, options) \
                        VALUES (?, ?, ?, ?, ?, ?, ?);",
                      [
                        infodict.id,
                        infodict.name,
                        infodict.host,
                        infodict.port,
                        infodict.password,
                        infodict.type,
                        infodict.options
                      ]);
        tx.executeSql("COMMIT;");
    });
}

function deleteConnection(db, id) {
    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM connections WHERE id = ?;",
                      [
                        id
                      ]);
        tx.executeSql("COMMIT;");
    });
}
