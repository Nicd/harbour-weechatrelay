/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef WEECHATPROTOCOLHANDLER_H
#define WEECHATPROTOCOLHANDLER_H

#include "protocolhandler.h"

#include <utility>
#include <QObject>
#include <QDateTime>
#include <QHash>
#include <QPair>
#include <QVariant>

class WeeChatProtocolHandler : public ProtocolHandler
{
    Q_OBJECT
public:
    enum Type {
        CHAR,
        INTEGER,
        LONG,
        STRING,
        BUFFER,
        POINTER,
        TIME,
        HASHTABLE,
        HDATA,
        INFO,
        INFOLIST,
        ARRAY
    };

    Q_ENUMS(Type)

    explicit WeeChatProtocolHandler(QObject* parent = 0);
    void initialize(QString password);

    // How many bytes at least each message contains
    static const qint64 HEADER_BYTES = 5;


    // Individual object types
    static QVariant handleDynamicType(Type type, QDataStream* data);
    static QString handleChar(QDataStream* data);
    static qint32 handleInt(QDataStream* data);
    static qint64 handleLong(QDataStream* data);
    static QString handleString(QDataStream* data);
    static QByteArray handleRawBuffer(QDataStream* data);
    static QVariantList handleBuffer(QDataStream* data);
    static QString handlePointer(QDataStream* data);
    static QDateTime handleTime(QDataStream* data);
    static QVariantMap handleHashTable(QDataStream* data);
    static QVariantMap handleHdata(QDataStream* data);
    static QVariantMap handleInfo(QDataStream* data);
    static QVariantMap handleInfoList(QDataStream* data);
    static QVariantList handleArray(QDataStream* data);

    static Type readType(QDataStream* data);
    static Type stringToType(QString type);

signals:
    void newEvent(QString id, QVariant data);

public slots:
    void handleNewData(RelayConnection* connection);
    void sendDebugData(QString string);

protected:
    void emitData(QString data);
    void handleBody(QDataStream* data);

    // WeeChat event types
    void handlePong(QString id, QDataStream* data);
    void handleUpgrade(QString id);
    void handleUpgradeEnded(QString id);
    void handleOtherEvent(QString id, QDataStream* data);

    bool readingBody;

};

#endif // WEECHATPROTOCOLHANDLER_H
