/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include "relayconnection.h"

RelayConnection::RelayConnection(QTcpSocket* socket, QObject* parent):
    QObject(parent), socket(socket)
{
    socket->setParent(this);

    QObject::connect(socket, &QTcpSocket::connected, this, &RelayConnection::socketConnected);
    QObject::connect(socket, &QTcpSocket::readyRead, this, &RelayConnection::socketReadyRead);
    QObject::connect(socket, &QTcpSocket::disconnected, this, &RelayConnection::socketDisconnected);
}

QByteArray RelayConnection::read(quint64 bytes)
{
    char* data = new char[bytes];
    qint64 errorStatus = socket->read(data, bytes);

    if (errorStatus < 0)
    {
        // Reading from socket failed, disconnect automatically
        disconnect();
        emit disconnected();
        return QByteArray("");
    }

    return QByteArray(data, bytes);
}

qint64 RelayConnection::bytesAvailable() const
{
    return socket->bytesAvailable();
}

void RelayConnection::connect(QString host, uint port)
{
    socket->connectToHost(host, port);
}

void RelayConnection::disconnect()
{
    socket->disconnectFromHost();
}

void RelayConnection::write(QByteArray data)
{
    socket->write(data);
}

// A successful connection has been initiated
void RelayConnection::socketConnected()
{
    emit connected();
}

void RelayConnection::socketDisconnected()
{
    emit disconnected();
}

// There is new data available for reading on the socket
void RelayConnection::socketReadyRead()
{
    emit newDataAvailable();
}

