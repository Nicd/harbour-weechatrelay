/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include <QDebug>
#include <QDataStream>
#include <QQmlEngine>
#include <QStringList>
#include <QJsonDocument>

#include "weechatprotocolhandler.h"
#include "weechat/dynamictypeexception.h"

WeeChatProtocolHandler::WeeChatProtocolHandler(QObject* parent) :
    ProtocolHandler(parent),
    readingBody(false)
{
    bytesNeeded = HEADER_BYTES;
}

void WeeChatProtocolHandler::initialize(QString password)
{
    emitData("init compression=off,password=" + password);
    emitData("(init) info version");
    emitData("(starttest) test");
}


// INDIVIDUAL OBJECT TYPES

QVariant WeeChatProtocolHandler::handleDynamicType(Type type, QDataStream* data)
{
    if (type == CHAR)
    {
        return QVariant(handleChar(data));
    }
    else if (type == INTEGER)
    {
        return QVariant(handleInt(data));
    }
    else if (type == LONG)
    {
        return QVariant(handleLong(data));
    }
    else if (type == STRING)
    {
        return QVariant(handleString(data));
    }
    else if (type == BUFFER)
    {
        return QVariant(handleBuffer(data));
    }
    else if (type == POINTER)
    {
        return QVariant(handlePointer(data));
    }
    else if (type == TIME)
    {
        return QVariant(handleTime(data));
    }
    else if (type == HASHTABLE)
    {
        return QVariant(handleHashTable(data));
    }
    else if (type == HDATA) {
        return QVariant(handleHdata(data));
    }
    else if (type == ARRAY)
    {
        return QVariant(handleArray(data));
    }
    else if (type == INFO)
    {
        return QVariant(handleInfo(data));
    }
    else if (type == INFOLIST)
    {
        return QVariant(handleInfoList(data));
    }
}

QString WeeChatProtocolHandler::handleChar(QDataStream* data)
{
    quint8 c;
    *data >> c;
    return QString(static_cast<char>(c));
}

qint32 WeeChatProtocolHandler::handleInt(QDataStream* data)
{
    qint32 ret;
    *data >> ret;
    return ret;
}

qint64 WeeChatProtocolHandler::handleLong(QDataStream* data)
{
    // Long is stored as a string, with one byte showing the length
    qint64 ret = 0;
    quint8 length;
    *data >> length;

    quint8 current;
    qint8 magnitude = 1;
    for (quint8 i = 0; i < length; ++i)
    {
        *data >> current;

        if (i == 0 && current == '-')
        {
            magnitude = -1;
        }
        else
        {
            ret *= 10;
            ret += magnitude * static_cast<qint64>(current - '0');
        }
    }

    return ret;
}

QString WeeChatProtocolHandler::handleString(QDataStream* data)
{
    // Strings are just buffers interpreted as string data
    QByteArray bytes = handleRawBuffer(data);

    // Lovely WeeChat sends everything our way in UTF-8 <3
    return QString::fromUtf8(bytes);
}

QByteArray WeeChatProtocolHandler::handleRawBuffer(QDataStream* data)
{
    // Buffers consist of a length (4 bytes) + data
    QByteArray ret;
    quint32 length;
    *data >> length;

    // If length is all 1s, the content is empty
    if (length == 0xffffffff)
    {
        return QByteArray();
    }

    quint8 current;
    for (quint32 i = 0; i < length; ++i)
    {
        *data >> current;
        ret.append(static_cast<char>(current));
    }

    return ret;
}

QVariantList WeeChatProtocolHandler::handleBuffer(QDataStream* data)
{
    QByteArray rawData = handleRawBuffer(data);
    QVariantList ret;

    for (int i = 0; i < rawData.length(); ++i) {
        char b = rawData.at(i);
        ret.append(static_cast<quint8>(b));
    }

    return ret;
}

QString WeeChatProtocolHandler::handlePointer(QDataStream* data)
{
    // Pointers are stored as length (1 byte) + data as hex string
    QString ret;
    quint8 length;
    *data >> length;

    qDebug() << "Pointer length: " << length;

    quint8 current;
    for (quint8 i = 0; i < length; ++i)
    {
        *data >> current;

        // Convert 0 to '0' to handle null pointer
        if (current == 0)
        {
            current = '0';
        }

        ret.append(static_cast<char>(current));
    }

    qDebug() << "Pointer data: " << ret;

    return ret;
}

QDateTime WeeChatProtocolHandler::handleTime(QDataStream* data)
{
    // Time is stored as length (1 byte) + data (unix time)
    // It's the same format as a Long so we will read it that way
    QDateTime ret;
    quint64 timestamp = static_cast<quint64>(handleLong(data));
    return ret.fromMSecsSinceEpoch(timestamp * 1000);
}

QVariantMap WeeChatProtocolHandler::handleHashTable(QDataStream* data)
{
    QVariantMap ret;

    // The hashtable first contains the types for the key and value
    Type keyType = readType(data);
    Type valueType = readType(data);

    Q_ASSERT_X(keyType == STRING,
               "HashTable::readFrom", "HashTable uses non-string keys.");

    // Next is the length of the table as a 4-byte int
    qint32 length = 0;
    *data >> length;

    for (qint32 i = 0; i < length; ++i)
    {
        QString key = handleString(data);
        QVariant value = handleDynamicType(valueType, data);

        ret.insert(key, value);
    }

    return ret;
}

QVariantMap WeeChatProtocolHandler::handleHdata(QDataStream* data)
{
    /*
     * A Hdata consists of:
     * H-path, a string giving the path to the data
     * Keys, a dict of keys and their types (in a string, comma separated)
     * Integer count of object sets
     * N object sets which themselves consist of:
     * * P-path, list of pointers to objects (the same amount as the amount
     *   of parts in H-path
     * * List of objects, with the same keys and types as defined in Keys
     */

    QVariantMap hdata;
    QVariantList objectSets;
    QVariantMap keys;

    QString hPath = handleString(data);
    qDebug() << "H-path: " << hPath;

    QString keyString = handleString(data);
    qDebug() << "Keystr: " << keyString;
    QStringList keyList = keyString.split(",");
    QStringList orderedKeys;

    for (QString keyStr : keyList)
    {
        QStringList keySplit = keyStr.split(":");
        QString keyName = keySplit.at(0);
        QString keyTypeStr = keySplit.at(1);
        keys.insert(keyName, keyTypeStr);
        orderedKeys.append(keyName);
    }

    qint32 objectSetCount = handleInt(data);

    for (qint32 i = 0; i < objectSetCount; ++i)
    {
        QVariantMap objectSet;

        // The amount of pointers is the amount of parts in H-path,
        // separated by /
        QVariantList pointers;
        int pointerCount = hPath.split("/").size();
        qDebug() << "PointerCount: " << pointerCount;
        for (int r = 0; r < pointerCount; ++r)
        {
            pointers.append(handlePointer(data));
        }

        objectSet.insert(QString("__path"), pointers);

        for (QString keyName : orderedKeys)
        {
            Type type = stringToType(keys.value(keyName).toString());
            qDebug() << "Handling key " << keyName;
            objectSet.insert(keyName, handleDynamicType(type, data));
        }

        objectSets.append(objectSet);
    }

    hdata.insert("objectSets", objectSets);
    hdata.insert("keys", keys);
    hdata.insert("hpath", hPath);
    return hdata;
}

QVariantMap WeeChatProtocolHandler::handleInfo(QDataStream* data)
{
    // Info consists of a string key and a string value
    qDebug() << "Reading info";
    QVariantMap ret;
    QString key = handleString(data);
    qDebug() << "Key: " << key;
    QString value = handleString(data);
    qDebug() << "Value: " << value;

    ret.insert("key", key);
    ret.insert("value", value);
    return ret;
}

QVariantMap WeeChatProtocolHandler::handleInfoList(QDataStream* data)
{
    // Infolist consists of name, count and then name, type and value for each element
    QVariantMap ret;
    ret.insert("name", handleString(data));
    qint32 length = handleInt(data);

    QVariantMap values;
    for (qint32 i = 0; i < length; ++i)
    {
        QString key = handleString(data);
        Type type = readType(data);
        QVariant value = handleDynamicType(type, data);
        values.insert(key, value);
    }

    ret.insert("values", values);

    return ret;
}

QVariantList WeeChatProtocolHandler::handleArray(QDataStream* data)
{
    QVariantList ret;

    // Array consists of type (string), number of elements (int 4 bytes) and data
    Type type = readType(data);
    qint32 length = handleInt(data);

    for (qint32 i = 0; i < length; ++i)
    {
        ret.append(handleDynamicType(type, data));
    }

    return ret;
}

WeeChatProtocolHandler::Type WeeChatProtocolHandler::readType(QDataStream* data)
{
    // Type is stored as three chars
    QString type = "";
    type.append(handleChar(data));
    type.append(handleChar(data));
    type.append(handleChar(data));
    return stringToType(type);
}

WeeChatProtocolHandler::Type WeeChatProtocolHandler::stringToType(QString type)
{
    if (type == "chr")
        return CHAR;
    else if (type == "int")
        return INTEGER;
    else if (type == "lon")
        return LONG;
    else if (type == "str")
        return STRING;
    else if (type == "buf")
        return BUFFER;
    else if (type == "ptr")
        return POINTER;
    else if (type == "tim")
        return TIME;
    else if (type == "htb")
        return HASHTABLE;
    else if (type == "hda")
        return HDATA;
    else if (type == "inf")
        return INFO;
    else if (type == "inl")
        return INFOLIST;
    else if (type == "arr")
        return ARRAY;

    else
    {
        qDebug() << "Unknown type: " << type;
        throw new DynamicTypeException("Unknown type!");
    }
}



// Handle incoming data from the socket and emit the appropriate signals
void WeeChatProtocolHandler::handleNewData(RelayConnection* connection)
{
    // If we cannot read the amount of bytes we want, skip and retry next time
    if (connection->bytesAvailable() < bytesNeeded)
    {
        qDebug() << connection->bytesAvailable() << " was lower than " << bytesNeeded << "... Skipping.";
        return;
    }

    QDataStream* data = read(connection, bytesNeeded);

    if (!readingBody)
    {
        // The header contains the length of the message and compression specifier
        quint32 length;
        quint8 compression;

        (*data) >> length;
        (*data) >> compression;

        qDebug() << length << " " << compression;

        // The length includes the header
        readingBody = true;
        bytesNeeded = length - HEADER_BYTES;

        // Attempt to read the body right away
        handleNewData(connection);
    }
    else
    {
        handleBody(data);
        readingBody = false;
        bytesNeeded = HEADER_BYTES;
    }

    delete data;
    data = 0;
}

void WeeChatProtocolHandler::sendDebugData(QString string)
{
    emitData(string);
}



void WeeChatProtocolHandler::emitData(QString data)
{
    ProtocolHandler::emitData(data + "\n");
}

void WeeChatProtocolHandler::handleBody(QDataStream* data)
{
    // The first element is the id which will affect how we interpret the rest
    char* dataArr = 0;
    (*data) >> dataArr;
    QString id(dataArr);

    qDebug() << "ID: " << id;

    if (id == "_pong")
    {
        handlePong(id, data);
    }
    else if (id == "_upgrade")
    {
        handleUpgrade(id);
    }
    else if (id == "_upgrade_ended")
    {
        handleUpgradeEnded(id);
    }
    else
    {
        handleOtherEvent(id, data);
    }

    delete[] dataArr;
    dataArr = 0;
}


// EVENTS

void WeeChatProtocolHandler::handlePong(QString id, QDataStream* data)
{
    QString response = handleString(data);
    emit newEvent(id, response);
}

void WeeChatProtocolHandler::handleUpgrade(QString id)
{
    // There is no data to read for this message
    emit newEvent(id, QVariant());
}

void WeeChatProtocolHandler::handleUpgradeEnded(QString id)
{
    // There is no data to read for this message
    emit newEvent(id, QVariant());
}

void WeeChatProtocolHandler::handleOtherEvent(QString id, QDataStream* data)
{
    // In this case we don't know the structure of the event, so we will
    // read all incoming types into a list
    QVariantList ret;

    while (!data->atEnd())
    {
        Type type = readType(data);

        QVariant obj = handleDynamicType(type, data);
        ret.append(obj);
    }

    emit newEvent(id, ret);
}
