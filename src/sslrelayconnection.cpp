#include "sslrelayconnection.h"
#include "connectresolver.h"

#include <QStringList>
#include <QSslCertificate>
#include <QSslKey>
#include <QMetaObject>

SSLRelayConnection::SSLRelayConnection(QSslSocket* socket, QObject* parent) :
    RelayConnection(socket, parent), sslSocket(socket)
{
    QObject::connect(socket, SELECT<const QList<QSslError>&>::OVERLOAD_OF(&QSslSocket::sslErrors),
                     this, &SSLRelayConnection::socketSslErrors);

    // Disable SSLv3 as it is old and vulnerable. This should use the protocols deemed safe by
    // the Qt version deployed on the device.
    sslSocket->setProtocol(QSsl::SecureProtocols);
}

void SSLRelayConnection::setHandler(ConnectionHandler* handler)
{
    this->handler = handler;
}



void SSLRelayConnection::connect(QString host, uint port)
{
    sslSocket->connectToHostEncrypted(host, port);
}



void SSLRelayConnection::socketSslErrors(const QList<QSslError>& errors)
{
    QStringList errorStrings;
    for (QSslError error : errors)
    {
        errorStrings.append(error.errorString());
    }

    QSslCertificate cert = sslSocket->peerCertificate();

    // Let the user know about the errors and then fail the connection if the certificate
    // wasn't accepted
    bool accepted = false;
    QMetaObject::invokeMethod(handler,
                              "relaySslErrors",
                              Qt::AutoConnection,
                              Q_RETURN_ARG(bool, accepted),
                              Q_ARG(QStringList, errorStrings),
                              Q_ARG(QSslCertificate, cert));

    if (accepted)
    {
        sslSocket->ignoreSslErrors(errors);
    }
}
