/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include <QTcpSocket>
#include <QSslSocket>
#include <QMetaObject>
#include <QDateTime>
#include <QString>

#include "connectionhandler.h"
#include "sslrelayconnection.h"
#include "weechatprotocolhandler.h"

ConnectionHandler::ConnectionHandler(QObject* parent) :
    QObject(parent),
    connection(0),
    handler(0),
    protocol(WEECHAT),
    security(NONE),
    host(),
    port(0),
    password(),
    acceptInfo(0),
    failedCert()
{
}


void ConnectionHandler::connect(ProtocolType type,
                                ConnectionSecurity security,
                                QString host,
                                uint port,
                                QString password)
{
    this->host = host;
    this->port = port;
    this->password = password;
    this->protocol = type;
    this->security = security;

    if (type == ProtocolType::WEECHAT) {
        handler = new WeeChatProtocolHandler(this);
    }

    if (security == ConnectionSecurity::NONE) {
        QTcpSocket* socket = new QTcpSocket(this);
        connection = new RelayConnection(socket, 0);
    }
    else if (security == ConnectionSecurity::SSL) {
        QSslSocket* socket = new QSslSocket(this);
        SSLRelayConnection* sslConnection = new SSLRelayConnection(socket, 0);
        sslConnection->setHandler(this);
        connection = sslConnection;
    }

    connectSignals();

    // Move connection to another thread to prevent blocking the rest of the app
    //connection->moveToThread(&connectionThread);
    //connectionThread.start();

    QMetaObject::invokeMethod(connection,
                              "connect",
                              Qt::AutoConnection,
                              Q_ARG(QString, host),
                              Q_ARG(uint, port));
}


void ConnectionHandler::disconnect()
{
    connection->disconnect();
    delete handler;
    handler = 0;
}

void ConnectionHandler::reconnect()
{
    connect(protocol, security, host, port, password);
}

// Reconnect with the certificate that failed to verify the last time
void ConnectionHandler::reconnectWithFailed()
{
    acceptInfo = new QSslCertificateInfo(failedCert, this);
    reconnect();
}

// Return the failed certificate for storage in PEM form
QSslCertificateInfo* ConnectionHandler::getFailedCertificate()
{
    return new QSslCertificateInfo(failedCert);
}

// Parse and accept the given certificate on next connection
// Note: Will not use the given certificate if the dates are wrong
void ConnectionHandler::acceptCertificate(QSslCertificateInfo* info)
{
    // Check that the current date is between the start and end dates
    QDateTime now = QDateTime::currentDateTime();
    if (now >= info->getEffectiveDate() && now <= info->getExpiryDate())
    {
        info->setParent(this);
        acceptInfo = info;
    }
    else
    {
        emit displayDebugData("Stored SSL certificate skipped due to invalid dates.");
    }
}

// Clear all meaningful data left over by previous connection attempts
void ConnectionHandler::clearData()
{
    protocol = WEECHAT;
    security = NONE;
    host = "";
    port = 0;
    password = "";
    acceptInfo = 0;
    failedCert = QSslCertificate();
}



void ConnectionHandler::relayConnected()
{
    emit connected();

    handler->initialize(password);
}

void ConnectionHandler::relayDisconnected()
{
    emit disconnected();
}

bool ConnectionHandler::relaySslErrors(QStringList errorStrings, QSslCertificate remoteCert)
{
    // The SSL verification failed, check if we can accept the certificate anyway

    if (acceptInfo != 0)
    {
        QString remoteDigest(remoteCert.digest(QCryptographicHash::Sha3_512).toHex());

        if (acceptInfo->getEffectiveDate() == remoteCert.effectiveDate()
                && acceptInfo->getExpiryDate() == remoteCert.expiryDate()
                && acceptInfo->getDigest() == remoteDigest)
        {
            return true;
        }
    }

    QStringList issuerInfoList = remoteCert.issuerInfo(QSslCertificate::Organization);

    failedCert = remoteCert;
    emit sslError(errorStrings.join("\n"), issuerInfoList.join("\n"),
                  remoteCert.effectiveDate(), remoteCert.expiryDate(),
                  remoteCert.digest(QCryptographicHash::Sha256).toHex());
    return false;
}


void ConnectionHandler::handleDebugData(QString hexstring)
{
    emit displayDebugData(hexstring);
}

void ConnectionHandler::sendDebugData(QString string)
{

}

void ConnectionHandler::send(QString string)
{
    string = string.append("\n");
    connection->write(string.toUtf8());
}

void ConnectionHandler::connectSignals()
{
    QObject::connect(connection, &RelayConnection::connected, this, &ConnectionHandler::relayConnected);
    QObject::connect(connection, &RelayConnection::disconnected, this, &ConnectionHandler::relayDisconnected);

    QObject::connect(connection, &RelayConnection::newDataAvailable, [this]() {
        this->handler->handleNewData(connection);
    });

    QObject::connect(handler, &ProtocolHandler::debugData, this, &ConnectionHandler::handleDebugData);
    QObject::connect(handler, &ProtocolHandler::sendData, connection, &RelayConnection::write);
    QObject::connect(handler, SIGNAL(newEvent(QString,QVariant)), this, SIGNAL(newEvent(QString,QVariant)));
}
