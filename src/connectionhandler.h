/*
 * © Mikko Ahlroth 2014
 * WeeCRApp is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef CONNECTIONHANDLER_H
#define CONNECTIONHANDLER_H

#include <QObject>
#include <QThread>
#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QSslCertificate>
#include <QByteArray>

#include "relayconnection.h"
#include "protocolhandler.h"
#include "qsslcertificateinfo.h"

class ConnectionHandler : public QObject
{
    Q_OBJECT

public:
    enum ConnectionSecurity { NONE, SSL };
    enum ProtocolType { WEECHAT };

    Q_ENUMS(ConnectionSecurity ProtocolType)

    explicit ConnectionHandler(QObject* parent = 0);

    Q_INVOKABLE void connect(ProtocolType protocol,
                             ConnectionSecurity security,
                             QString host,
                             uint port,
                             QString password);
    Q_INVOKABLE void disconnect();
    Q_INVOKABLE void reconnect();
    Q_INVOKABLE void reconnectWithFailed();
    Q_INVOKABLE QSslCertificateInfo* getFailedCertificate();
    Q_INVOKABLE void acceptCertificate(QSslCertificateInfo* info);
    Q_INVOKABLE void clearData();

signals:
    void connected();
    void disconnected();

    void displayDebugData(QString data);
    void newEvent(QString id, QVariant data);
    void sslError(QString errorStrings, QString issuerInfo,
                  QDateTime startDate, QDateTime expiryDate,
                  QString digest);

public slots:
    void relayConnected();
    void relayDisconnected();
    bool relaySslErrors(QStringList errorStrings, QSslCertificate remoteCert);

    void handleDebugData(QString hexstring);
    void sendDebugData(QString string);
    void send(QString string);

private:
    RelayConnection* connection;
    ProtocolHandler* handler;
    ProtocolType protocol;
    ConnectionSecurity security;
    QString host;
    uint port;
    QString password;

    // Accept SSL certificates with this info when connecting
    QSslCertificateInfo* acceptInfo;

    // The SSL certificate that failed verification on last connect
    QSslCertificate failedCert;


    void connectSignals();
};

#endif // CONNECTIONHANDLER_H
